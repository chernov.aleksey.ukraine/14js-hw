let season;

const makeDay = () => {
    document.querySelectorAll('.night').forEach((elem) => {
        elem.classList.add('day');
        elem.classList.remove('night');
    });
};

if (localStorage.getItem('season')) {
    season = localStorage.getItem('season')
} else {season = 'night'};
if (season === 'day') {makeDay()}; 
       
document.querySelector('button').addEventListener('click', () => {
    if (season === 'night') {
        makeDay();
        localStorage.setItem('season', 'day');
        season = 'day';
    } else {
        document.querySelectorAll('.day').forEach((elem) => {
            elem.classList.add('night');
            elem.classList.remove('day');
        })
        localStorage.setItem('season', 'night');
        season = 'night';
    };
}); 
